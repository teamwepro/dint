<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});

Auth::routes(['register' => false]);


/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
*/

Route::get('dashboard','DashboardController@dashboard')->name('dashboard');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'prefix' => 'roles',
], function () {
    Route::get('/', 'RolesController@newindex')
         ->name('roles.role.index');
    Route::get('/create','RolesController@create')
         ->name('roles.role.create');
    Route::get('/show/{role}','RolesController@show')
         ->name('roles.role.show')->where('id', '[0-9]+');
    Route::get('/{role}/edit','RolesController@edit')
         ->name('roles.role.edit')->where('id', '[0-9]+');
    Route::post('/', 'RolesController@store')
         ->name('roles.role.store');
    Route::put('role/{role}', 'RolesController@update')
         ->name('roles.role.update')->where('id', '[0-9]+');
    Route::delete('/role/{role}','RolesController@destroy')
         ->name('roles.role.destroy')->where('id', '[0-9]+');
});

Route::group([
    'prefix' => 'permissions',
], function () {
    Route::get('/', 'PermissionsController@index')
         ->name('permissions.permission.index');
    Route::get('/create','PermissionsController@create')
         ->name('permissions.permission.create');
    Route::get('/show/{permission}','PermissionsController@show')
         ->name('permissions.permission.show')->where('id', '[0-9]+');
    Route::get('/{permission}/edit','PermissionsController@edit')
         ->name('permissions.permission.edit')->where('id', '[0-9]+');
    Route::post('/', 'PermissionsController@store')
         ->name('permissions.permission.store');
    Route::put('permission/{permission}', 'PermissionsController@update')
         ->name('permissions.permission.update')->where('id', '[0-9]+');
    Route::delete('/permission/{permission}','PermissionsController@destroy')
         ->name('permissions.permission.destroy')->where('id', '[0-9]+');
});

Route::group([
    'prefix' => 'users',
], function () {
    Route::get('/', 'UsersController@index')
         ->name('users.user.index');
    Route::get('/create','UsersController@create')
         ->name('users.user.create');
    Route::get('/show/{user}','UsersController@show')
         ->name('users.user.show');
    Route::get('/{user}/edit','UsersController@edit')
         ->name('users.user.edit');
    Route::post('/', 'UsersController@store')
         ->name('users.user.store');
    Route::put('user/{user}', 'UsersController@update')
         ->name('users.user.update');
    Route::delete('/user/{user}','UsersController@destroy')
         ->name('users.user.destroy');
});


Route::group([
    'prefix' => 'settings',
    'middleware' => ['auth'],
], function () {
    Route::get('/', 'DotenvEditorController@index')
         ->name('settings.dotenv_editor.index');
    Route::get('update/{line}', 'DotenvEditorController@update')
         ->name('settings.dotenv_editor.update')->where('id', '[0-9]+');
    Route::delete('destroy','DotenvEditorController@destroy')
         ->name('settings.dotenv_editor.destroy')->where('id', '[0-9]+');
   
});