@extends('layouts.dashboard')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Settings</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
               
            </div>

        </div>
        
        @if(count($lines) == 0)
            <div class="panel-body text-center">
                <h4>No Settings Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table id="data-table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Key</th>
                            <th>Value</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($lines as $line)
                    @if( $line['parsed_data']['type'] == 'setter')
                        <tr>
                            <td>{{ $line['parsed_data']['key'] }}</td>
                            <td>{{ $line['parsed_data']['value'] }}</td>

                            <td>

                                <form method="POST" action="{!! route('settings.dotenv_editor.destroy', $line['line']) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">

                                        <a href="{{ route('settings.dotenv_editor.update', $line['line'] ) }}" class="btn btn-primary" title="Edit Setting">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Settings" onclick="return confirm(&quot;Click Ok to delete setting.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endif
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
           
        </div>
        
        @endif
    
    </div>
@endsection