<!DOCTYPE html>
<html lang="en" dir="ltr">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Hero</title>
      <!-- Prevent the demo from appearing in search engines -->
      <meta name="robots" content="noindex">
      <!-- App CSS -->
      <link type="text/css" href="assets/css/app.css" rel="stylesheet">
      <link type="text/css" href="assets/css/app.rtl.css" rel="stylesheet">
      <!-- Simplebar -->
      <link type="text/css" href="assets/vendor/simplebar.css" rel="stylesheet">
   </head>
   <body>
      <div class="mdk-drawer-layout js-mdk-drawer-layout" data-fullbleed data-push data-has-scrolling-region>
         <div class="mdk-drawer-layout__content mdk-header-layout__content--scrollable" style="overflow-y: auto;" data-simplebar data-simplebar-force-enabled="true">
            <div class="container h-vh d-flex justify-content-center align-items-center flex-column">
               <div class="d-flex justify-content-center align-items-center mb-3">
                  <a href="#" class="drawer-brand-circle mr-2">H</a>
                  <h2 class="ml-2 text-bg mb-0"><strong>{{ __('Login') }}</strong></h2>
               </div>
               <div class="row w-100 justify-content-center">
                  <div class="card card-login mb-3">
                     <div class="card-body">
                        <form method="POST" action="{{ route('login') }}" >
                           @csrf
                           <div class="form-group">
                              <label>{{ __('E-Mail Address') }}</label>
                              <div class="input-group input-group--inline">
                                 <div class="input-group-addon">
                                    <i class="material-icons">account_circle</i>
                                 </div>
                                 <input id="email"  type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                 @error('email')
                                 <span class="invalid-feedback" role="alert">
                                 <strong>{{ $message }}</strong>
                                 </span>
                                 @enderror
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="d-flex">
                                 <label for="password">{{ __('Password') }}</label>
                                 <span class="ml-auto">
                                 @if (Route::has('password.request'))<a href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a> 
                                 @endif
                                 </span>
                              </div>
                              <div class="input-group input-group--inline">
                                 <div class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                 </div>
                                 <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                 @error('password')
                                 <span class="invalid-feedback" role="alert">
                                 <strong>{{ $message }}</strong>
                                 </span>
                                 @enderror
                              </div>
                           </div>
                           <div class="text-center">
                              <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
               <!--div class="form-group row">
                  <div class="col-md-6 offset-md-4">
                     <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                        </label>
                     </div>
                  </div>
               </div-->
               <!--div class="d-flex justify-content-center">
                  <span class="mr-2">Don't have an account?</span>
                  <a href="signup.html">Sign Up</a>
               </div-->
            </div>
         </div>
      </div>
      <script>
         (function() {
             'use strict';
         
             // Self Initialize DOM Factory Components
             domFactory.handler.autoInit();
         });
      </script>
   </body>
</html>
<!--     <div class="row justify-content-center">
   <div class="col-md-8">
       <div class="card">
           <div class="card-header">{{ __('Login') }}</div>
   
           <div class="card-body">
               <form method="POST" action="{{ route('login') }}">
                   @csrf
   
                   <div class="form-group row">
                       <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
   
                       <div class="col-md-6">
                           <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
   
                           @error('email')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                       </div>
                   </div>
   
                   <div class="form-group row">
                       <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
   
                       <div class="col-md-6">
                           <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
   
                           @error('password')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                       </div>
                   </div>
   
                   <div class="form-group row">
                       <div class="col-md-6 offset-md-4">
                           <div class="form-check">
                               <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
   
                               <label class="form-check-label" for="remember">
                                   {{ __('Remember Me') }}
                               </label>
                           </div>
                       </div>
                   </div>
   
                   <div class="form-group row mb-0">
                       <div class="col-md-8 offset-md-4">
                           <button type="submit" class="btn btn-primary">
                               {{ __('Login') }}
                           </button>
   
                           @if (Route::has('password.request'))
                               <a class="btn btn-link" href="{{ route('password.request') }}">
                                   {{ __('Forgot Your Password?') }}
                               </a>
                           @endif
                       </div>
                   </div>
               </form>
           </div>
       </div>
   </div>
   </div>
   </div> -->