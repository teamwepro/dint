<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jackiedo\DotenvEditor\DotenvEditor;
use Alert;

class DotenvEditorController extends Controller
{
    protected $editor;

    public function __construct(DotenvEditor $editor)
    {
        $this->editor = $editor;
    }

    public function index()
    {
        $lines  = $this->editor->getLines();
        return view('settings.index', compact('lines'));

    }

    public function update($line, Request $request)
    {
    	die('Under construction');
        $lines  = $this->editor->getLines();

        return view('seetings.update');

    }

    public function destroy()
    {
       try {
            
            return redirect()->route('settings.dotenv_editor.index')
                ->with('success_message', 'Setting was successfully deleted.')
                ->with('success', 'Profile updated!');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }

    }
}
