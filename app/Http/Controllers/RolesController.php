<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;
use Exception;
use Alert;
class RolesController extends Controller
{

    /**
     * Display a listing of the roles.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $roles = Role::paginate(25);

        return view('roles.index', compact('roles'));
    }

    /**
     * Display a listing of the roles.
     *
     * @return Illuminate\View\View
     */
    public function newindex()
    {
       
        $roles       = Role::all();
        $permissions = Permission::all();

        return view('roles.newindex', compact('roles', 'permissions'));

        
    }

    /**
     * Store a new role in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */

    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|unique:roles']);

        if( Role::create($request->only('name')) ) {
            return redirect()->route('roles.role.index')
                ->with('success', 'Role was successfully added.');
        }

        return redirect()->back();
    }

    /**
     * Update the specified role in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    
    public function update($id, Request $request)
    {
        if($role = Role::findOrFail($id)) {
            // admin role has everything
            if($role->name === 'Admin') {
                $role->syncPermissions(Permission::all());
                return redirect()->route('roles.role.index');
            }

            $permissions = $request->get('permissions', []);
            $role->syncPermissions($permissions);
            Alert::success($role->name . ' permissions has been updated.');
        } else {
            Alert::error( 'Role with id '. $id .' note found.');
        }

        return redirect()->route('roles.role.index');
    }

    /**
     * Remove the specified role from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $role = Role::findOrFail($id);
            $role->delete();

            return redirect()->route('roles.role.index')
                ->with('success_message', 'Role was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


}
